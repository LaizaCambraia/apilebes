package com.clientes.apiLebes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiLebesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiLebesApplication.class, args);
	}

}
