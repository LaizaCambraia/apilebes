package com.clientes.apiLebes.Controllers;

import com.clientes.apiLebes.Entities.Cliente;
import com.clientes.apiLebes.Models.ClienteRequest;
import com.clientes.apiLebes.Models.ResponseRequest;
import com.clientes.apiLebes.Repositories.ClienteRepositorie;
import com.clientes.apiLebes.Services.IClienteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value="API REST Lebes")
@CrossOrigin(origins="*")
@RequestMapping(value="/apiLebes")
public class ClienteController {

    @Autowired
    private IClienteService iClienteService;

    @ApiOperation(value = "Retorna uma lista de clientes")
    @GetMapping("/clientes")
    public List<Cliente> GetClientes(){
        return iClienteService.GetClientes();
    }

    @GetMapping("/cliente/{id}")
    @ApiOperation(value = "Retorna um cliente unico")
    public Cliente GetClienteId(@PathVariable(value="id") long id){
        return iClienteService.GetClienteId(id);
    }

    @PutMapping("/inativar-cliente/{id}")
    @ApiOperation(value = "Inativar cliente")
    public ResponseRequest PutCliente(@PathVariable(value="id") long id){
        return iClienteService.PutCliente(id);
    }

    @PostMapping("/cliente")
    @ApiOperation(value = "Salva um cliente")
    public ResponseRequest PostCliente(@RequestBody ClienteRequest clienteRequest){
        return iClienteService.PostCliente(clienteRequest);
    }

    @DeleteMapping("/cliente/{id}")
    @ApiOperation(value = "Deleta um cliente")
    public ResponseRequest DeleteCliente(@PathVariable(value="id") long id){
        return iClienteService.DeleteCliente(id);
    }

    @PutMapping("/cliente")
    @ApiOperation(value = "Atualiza um cliente")
    public ResponseRequest PutCliente(@RequestBody ClienteRequest clienteRequest){
        return iClienteService.PutCliente(clienteRequest);
    }
}
