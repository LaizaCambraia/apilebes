package com.clientes.apiLebes.Repositories;

import com.clientes.apiLebes.Entities.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepositorie extends JpaRepository<Cliente, Long> {

    Cliente findById(long id);
    Cliente findByCPF(String cpf);
}
