package com.clientes.apiLebes.Services;

import com.clientes.apiLebes.Entities.Cliente;
import com.clientes.apiLebes.Models.ClienteRequest;
import com.clientes.apiLebes.Models.ResponseRequest;
import com.clientes.apiLebes.Repositories.ClienteRepositorie;
import com.clientes.apiLebes.Validation.IValidationCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ClienteService implements IClienteService{

    @Autowired
    private ClienteRepositorie clienteRepositorie;
    @Autowired
    private IValidationCliente iValidationCliente;

    public List<Cliente> GetClientes(){
        return clienteRepositorie.findAll();
    }

    public Cliente GetClienteId(long id){
        return clienteRepositorie.findById(id);
    }

    public ResponseRequest PostCliente(ClienteRequest clienteRequest) {
        ResponseRequest responseRequest = new ResponseRequest();

        try{
            String validacaoMsg = iValidationCliente.Validacao(clienteRequest, "post");
            if(!validacaoMsg.equals(""))
            {
                responseRequest.Sucess = false;
                responseRequest.Mensagem = validacaoMsg;
                return responseRequest;
            }

            Cliente cliente = iValidationCliente.MapearCliente(clienteRequest);

            cliente.setStatus(true);
            clienteRepositorie.save(cliente);

            responseRequest.Sucess = true;
            responseRequest.Mensagem = "Cliente cadastrado com sucesso";
        }
        catch (Exception ex)
        {
            responseRequest.Sucess = false;
            responseRequest.Mensagem = "Ocorreu um erro ao realizar o cadastro.";
        }
        return responseRequest;
    }

    public ResponseRequest PutCliente(long id) {
        ResponseRequest responseRequest = new ResponseRequest();
        try{
            Cliente cliente = clienteRepositorie.findById(id);

            if(cliente != null){
                cliente.setStatus(false);
                clienteRepositorie.save(cliente);

                responseRequest.Sucess = true;
                responseRequest.Mensagem = "Cliente inativado com sucesso";
            }
            else{
                responseRequest.Sucess = false;
                responseRequest.Mensagem = "O cliente informado não existe.";
            }
        }
        catch (Exception ex)
        {
            responseRequest.Sucess = false;
            responseRequest.Mensagem = "Ocorreu um erro ao realizar o cadastro.";
        }
        return responseRequest;
    }

    public ResponseRequest DeleteCliente(long id){
        ResponseRequest responseRequest = new ResponseRequest();
        try{
            Cliente cliente = clienteRepositorie.findById(id);

            if(cliente != null){
                clienteRepositorie.delete(cliente);
                responseRequest.Sucess = true;
                responseRequest.Mensagem = "Cliente deletado com sucesso";
            }
            else {
                responseRequest.Sucess = false;
                responseRequest.Mensagem = "O cliente informado não existe.";
            }
        }
        catch (Exception ex)
        {
            responseRequest.Sucess = false;
            responseRequest.Mensagem = "Ocorreu um erro ao deletar o cadastro.";
        }
        return responseRequest;
    }

    public ResponseRequest PutCliente(ClienteRequest clienteRequest){
        ResponseRequest responseRequest = new ResponseRequest();
        try{

            String validacaoMsg = iValidationCliente.Validacao(clienteRequest, "put");
            if(!validacaoMsg.equals(""))
            {
                responseRequest.Sucess = false;
                responseRequest.Mensagem = validacaoMsg;
                return responseRequest;
            }

            Cliente cliente = iValidationCliente.MapearCliente(clienteRequest);
            Cliente clienteId = clienteRepositorie.findByCPF(clienteRequest.getCPF());
            cliente.setId(clienteId.getId());
            cliente.setStatus(clienteId.getStatus());

            clienteRepositorie.save(cliente);
            responseRequest.Sucess = true;
            responseRequest.Mensagem = "Cliente atualizado com sucesso";
        }
        catch (Exception ex)
        {
            responseRequest.Sucess = false;
            responseRequest.Mensagem = "Ocorreu um erro ao atualizar o cadastro.";
        }
        return responseRequest;
    }
}
