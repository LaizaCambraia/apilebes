package com.clientes.apiLebes.Services;

import com.clientes.apiLebes.Entities.Cliente;
import com.clientes.apiLebes.Models.ClienteRequest;
import com.clientes.apiLebes.Models.ResponseRequest;

import java.util.List;

public interface IClienteService {
    List<Cliente> GetClientes();
    Cliente GetClienteId(long id);
    ResponseRequest PostCliente(ClienteRequest clienteRequest);
    ResponseRequest PutCliente(long id);
    ResponseRequest PutCliente(ClienteRequest clienteRequest);
    ResponseRequest DeleteCliente(long id);
}
