package com.clientes.apiLebes.Validation;

import com.clientes.apiLebes.Entities.Cliente;
import com.clientes.apiLebes.Models.ClienteRequest;
import com.clientes.apiLebes.Repositories.ClienteRepositorie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Service
public class ValidationCliente implements IValidationCliente{

    @Autowired
    private ClienteRepositorie clienteRepositorie;

    public Boolean VerificaCliente(String CPF){
        Cliente cliente = clienteRepositorie.findByCPF(CPF);
        return cliente != null ? true : false;
    }

    public String VerificaCamposObrigatorios(ClienteRequest clienteRequest){
        String retorno = "";

        if(clienteRequest.getCPF() == null)
            retorno = "CPF";

        if(clienteRequest.getDataNascimento() == null)
            retorno += " Data de nascimento";

        if(clienteRequest.getNome() == null)
            retorno += " Nome.";

        return retorno;
    }

    public Boolean VerificaMAiorIdade(Date dataNascimento)
    {
        GregorianCalendar dateHj = new GregorianCalendar();
        GregorianCalendar nascimento = new GregorianCalendar();

        nascimento.setTime(dataNascimento);

        int idade = dateHj.get(Calendar.YEAR) - nascimento.get(Calendar.YEAR);

        return idade >= 18 ? true : false;
    }

    public String Validacao(ClienteRequest clienteRequest, String metodo)
    {
        String msg = "";
        String retorno = VerificaCamposObrigatorios(clienteRequest);

        if(retorno != "") {
            msg = "O(s) seguinte(s) campo(s) são obrigatorios: " + retorno;
        }
        else if(!VerificaMAiorIdade(clienteRequest.getDataNascimento())) {
            msg = "O cliente deve ser maior de idade.";
        }
        else if(VerificaCliente(clienteRequest.getCPF()) && metodo == "post") {
            msg = "Cliente já existe";
        }
        else if(metodo == "put") {
            Cliente cliente_validacao = clienteRepositorie.findByCPF(clienteRequest.getCPF());
            if(cliente_validacao == null)
                msg = "Cliente não existe.";
            else if(!cliente_validacao.getCPF().equals(clienteRequest.getCPF()))
                msg = "Não é permitido a atualização de CPF.";
        }

        return msg;
    }

    public Cliente MapearCliente(ClienteRequest clienteRequest){
        Cliente cliente = new Cliente();

        cliente.setCPF(clienteRequest.getCPF());
        cliente.setNome(clienteRequest.getNome());
        cliente.setDataNascimento(clienteRequest.getDataNascimento());
        cliente.setNomeMae(clienteRequest.getNomeMae());

        return cliente;
    }
}
