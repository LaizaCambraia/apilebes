package com.clientes.apiLebes.Validation;

import com.clientes.apiLebes.Entities.Cliente;
import com.clientes.apiLebes.Models.ClienteRequest;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public interface IValidationCliente  {
    public Boolean VerificaCliente(String CPF);
    public String VerificaCamposObrigatorios(ClienteRequest clienteRequest);
    public Boolean VerificaMAiorIdade(Date dataNascimento);
    public String Validacao(ClienteRequest clienteRequest, String metodo);
    public Cliente MapearCliente(ClienteRequest clienteRequest);
}
