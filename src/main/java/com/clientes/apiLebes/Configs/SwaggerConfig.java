package com.clientes.apiLebes.Configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static springfox.documentation.builders.PathSelectors.regex;

import java.util.ArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket apiLebes(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.clientes.apiLebes"))
                .paths(regex("/apiLebes.*"))
                .build()
                .apiInfo(metaInfo());
    }

    private ApiInfo metaInfo(){
        ApiInfo apiInfo = new ApiInfo(
                "Lebes API REST",
                "API REST de captção de clientes.",
                "1.0",
                "",
                new Contact("Laíza Cambraia", "https://www.linkedin.com/in/la%C3%ADza-cambraia-2213b640/", "laiza.cambraia@hotmail.com"),
                "",
                "", new ArrayList<VendorExtension>()
        );
        return apiInfo;
    }
}
